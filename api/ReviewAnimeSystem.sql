-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 03, 2022 at 06:14 PM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ReviewAnimeSystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `Anime`
--

CREATE TABLE `Anime` (
  `AnimeID` int(11) NOT NULL,
  `AnimeName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Anime`
--

INSERT INTO `Anime` (`AnimeID`, `AnimeName`) VALUES
(3, 'โกนหนวดไปทำงานแล้วกลับบ้านมาพบเธอ'),
(4, 'Your Name'),
(6, 'No Game No Life'),
(7, 'แมงมุมแล้วไงข้องใจหรอจ้ะ'),
(8, 'แมงมุมแล้วไงข้องใจหรอจ้ะ');

-- --------------------------------------------------------

--
-- Table structure for table `Post`
--

CREATE TABLE `Post` (
  `PostID` int(11) NOT NULL,
  `ReviewerID` int(11) NOT NULL,
  `AnimeID` int(11) NOT NULL,
  `PostContent` text NOT NULL,
  `PostTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Post`
--

INSERT INTO `Post` (`PostID`, `ReviewerID`, `AnimeID`, `PostContent`, `PostTime`) VALUES
(15, 5, 4, 'ภาพสวยงานดีย์', '2022-04-03 05:12:45'),
(17, 6, 4, 'นางเอกน่าร้ากกกมั่กๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆ', '2022-04-03 05:28:17');

-- --------------------------------------------------------

--
-- Table structure for table `Reviewer`
--

CREATE TABLE `Reviewer` (
  `ReviewerID` int(11) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Reviewer`
--

INSERT INTO `Reviewer` (`ReviewerID`, `Email`, `Username`, `Password`, `IsAdmin`) VALUES
(5, 'Narathorn.naja@gmail.com', 'Nara', '$2b$10$OjmEVo3AbFWn12JsE.wROOOYOAn/e/YquFXpnEBYs8HtzKWh.WH7e', 1),
(6, 'test06@gmail.com', 'test06', '$2b$10$0EwNMWC36fZp1Pup3GUQjul/Jpz9laUfVzYvDzt9lgY8QFl6ez9LG', 0),
(7, 'test07@gmail.com', 'test07', '$2b$10$JnTU8/EYnHn9rXGHuY5xS.2AlIySNV6ckrKo0tQROcZ9h6utv5YHa', 0),
(8, 'test08@gmail.com', 'test08', '$2b$10$YjUpGibsV2tjJ6rZDHQlFO0r9laONiWc8xsyUhk0LvD3GFYC81mZC', 0),
(9, 'test08@gmail.com', 'test08', '$2b$10$WqYjVdfXyf0ZkfIL8F2yiO6aOxO1K9MtNxHR7zL2U98XGgy.JcTJ2', 0),
(10, 'test09@gmail.com', 'test09', '$2b$10$1HbEYOQNIOqFOHh0xPJbB.0juSY/CGQdBtVZHeGtA4tXD7VUvm6/6', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Anime`
--
ALTER TABLE `Anime`
  ADD PRIMARY KEY (`AnimeID`);

--
-- Indexes for table `Post`
--
ALTER TABLE `Post`
  ADD PRIMARY KEY (`PostID`),
  ADD KEY `AnimeID` (`AnimeID`),
  ADD KEY `ReviewerID` (`ReviewerID`);

--
-- Indexes for table `Reviewer`
--
ALTER TABLE `Reviewer`
  ADD PRIMARY KEY (`ReviewerID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Anime`
--
ALTER TABLE `Anime`
  MODIFY `AnimeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `Post`
--
ALTER TABLE `Post`
  MODIFY `PostID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `Reviewer`
--
ALTER TABLE `Reviewer`
  MODIFY `ReviewerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Post`
--
ALTER TABLE `Post`
  ADD CONSTRAINT `AnimeID` FOREIGN KEY (`AnimeID`) REFERENCES `Anime` (`AnimeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ReviewerID` FOREIGN KEY (`ReviewerID`) REFERENCES `Reviewer` (`ReviewerID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
