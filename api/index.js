const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'reviewer_admin',
    password: 'reviewer_admin',
    database: 'ReviewAnimeSystem'
})

connection.connect()

const express = require('express')
const app = express()
const port = 4000

/* Middleware for Authenticating User Token */
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

app.get("/list_post", (req, res) => {
    let query = ` 
    SELECT Reviewer.Username, Anime.AnimeName, Post.PostContent, Post.PostTime
    FROM Reviewer, Post, Anime
    WHERE(Post.ReviewerID = Reviewer.ReviewerID) AND
         (Post.AnimeID = Anime.AnimeID) `

    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status": "400",
                "message": "Error querying from review anime db"
            })
        } else {
            res.json(rows)
        }
    })
})

app.get("/list_post_anime", (req, res) => {
    let anime_id = req.query.anime_id
    let query = ` 
    SELECT Anime.AnimeName, Reviewer.Username, Post.PostContent, Post.PostTime 
    FROM Reviewer, Post, Anime 
    WHERE(Post.ReviewerID = Reviewer.ReviewerID) AND 
    (Post.AnimeID = Anime.AnimeID) AND 
    (Anime.AnimeID = ${anime_id}) `

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error querying from review anime db"
            })
        } else {
            res.json(rows)
        }
    })
})

app.get("/list_post_user", (req, res) => {
    let user_name = req.query.user_name
    let query = ` 
    SELECT Reviewer.Username, Anime.AnimeName,  Post.PostContent, Post.PostTime 
    FROM Reviewer, Post, Anime 
    WHERE(Post.ReviewerID = Reviewer.ReviewerID) AND 
    (Post.AnimeID = Anime.AnimeID) AND 
    (Reviewer.Username = '${user_name}') `

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error querying from review anime db"
            })
        } else {
            res.json(rows)
        }
    })
})

app.get("/list_user", authenticateToken, (req, res) => {

    let query = ` SELECT * FROM Reviewer `

    if (!(req.user.IsAdmin)) {
        res.send("Unauthorized Because you're not admin")
    }
    else {
        connection.query(query, (err, rows) => {
            if (err) {
                console.log(err)
                res.json({
                    "status": "400",
                    "message": "Error querying from review anime db"
                })
            } else {
                res.json(rows)
            }
        })
    }
})

app.get("/list_anime", (req, res) => {
    let query = ` SELECT * FROM Anime `
    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error querying from review anime db"
            })
        } else {
            res.json(rows)
        }
    })
})

app.post("/post", authenticateToken, (req, res) => {
    let reviewer_id = req.user.user_id
    let anime_id = req.query.anime_id
    let content = req.query.content

    let query = `INSERT INTO Post 
                     (ReviewerID, AnimeID, PostContent, PostTime)
                    VALUES( '${reviewer_id}',
                            '${anime_id}',
                            '${content}',
                            NOW())`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status": "400",
                "message": "Error inserting data into db"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Adding post succesful"
            })
        }
    })
})

app.post("/edit_post", authenticateToken, (req, res) => {

    let reviewer_id = req.user.user_id
    let post_id = req.query.post_id
    let anime_id = req.query.anime_id
    let content = req.query.content
   
    let query = `UPDATE Post SET
                    AnimeID = ${anime_id},
                    PostContent = '${content}',
                    PostTime = NOW()
                    WHERE ReviewerID = ${reviewer_id} AND 
                          PostID = ${post_id}`

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error edit post"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Edit post succesful"
            })
        }
    })
})

app.post("/delete_post", authenticateToken, (req, res) => {

    let reviewer_id = req.user.user_id
    let post_id = req.query.post_id
    let query = `DELETE FROM Post 
                 WHERE PostID = ${post_id} AND
                 ReviewerID = ${reviewer_id}`

    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error deleting record"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Deleting record success"
            })
        }
    })
})

app.post("/add_anime", authenticateToken, (req, res) => {

    let anime_name = req.query.anime_name

    if (!(req.user.IsAdmin)) {
        res.send("Unauthorized Because you're not admin")
    }
    else{
        let query = `INSERT INTO Anime
                        (AnimeName)
                        VALUES('${anime_name}')`

        connection.query(query, (err, rows) => {
            if (err) {
                res.json({
                    "status": "400",
                    "message": "Error inserting data into db"
                })
            } else {
                res.json({
                    "status": "200",
                    "message": "Adding anime succesful"
                })
            }
        })
    }
})

app.post("/edit_anime", authenticateToken, (req, res) => {

    let anime_id = req.query.anime_id
    let anime_name = req.query.anime_name
    console.log(req.user)
    if(!(req.user.IsAdmin)){
        res.send("Unauthorized Because you're not admin")
    }
    else{
        let query = `UPDATE Anime SET
                        AnimeName = '${anime_name}'
                        WHERE AnimeID = ${anime_id}`

        connection.query(query, (err, rows) => {
            if (err) {
                console.log(err)
                res.json({
                    "status": "400",
                    "message": "Error edit anime name"
                })
            } else {
                res.json({
                    "status": "200",
                    "message": "Edit anime name succesful"
                })
            }
        })
    }
})

app.post("/delete_anime", authenticateToken, (req, res) => {

    let anime_id = req.query.anime_id
    console.log(req.user)
    if (!(req.user.IsAdmin)) {
        res.send("Unauthorized Because you're not admin")
    }
    else{
            let query = `DELETE FROM Anime 
                        WHERE AnimeID = ${anime_id}`

            console.log(query)

            connection.query(query, (err, rows) => {
                if (err) {
                    console.log(err)
                    res.json({
                        "status": "400",
                        "message": "Error deleting record"
                    })
                } else {
                    res.json({
                        "status": "200",
                        "message": "Deleting record success"
                    })
                }
            })
        }
})

app.post("/register", (req, res) => {
    let reviewer_username = req.query.reviewer_username
    let reviewer_email = req.query.reviewer_email
    let reviewer_password = req.query.reviewer_password

    bcrypt.hash(reviewer_password, SALT_ROUNDS, (err, hash) => {
        let query = `INSERT INTO Reviewer
                    (Username, Email, Password)
                    VALUES ('${reviewer_username}', 
                            '${reviewer_email}',
                            '${hash}')`

        connection.query(query, (err, rows) => {
            if (err) {
                res.json({
                    "status": "400",
                    "message": "Error inserting data into db"
                })
            } else {
                res.json({
                    "status": "200",
                    "message": "Adding new user succesful"
                })
            }
        })
    })
})

app.post("/login", (req, res) => {
    let username = req.query.username
    let user_password = req.query.user_password
    let query = `SELECT * FROM Reviewer WHERE Username = '${username}'`
    console.log(query)
    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status": "400",
                "message": "Error querying from Reviewer db"
            })
        } else {
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err, result) => {
                if (result) {
                    let payload = {
                        "user_id": rows[0].ReviewerID,
                        "username": rows[0].Username,
                        "Email": rows[0].Email,
                        "IsAdmin": rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn: '1d' })
                    res.send(token)
                } else {
                    res.send("Invalid username / password")
                }
            })
        }
    })
})

app.listen(port, () => {
    console.log('Now starting Review Anime System ' + port)
})