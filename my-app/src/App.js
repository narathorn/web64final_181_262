import './App.css';

import { Routes, Route } from "react-router-dom";
import Login from './components/Login'
import Header from './components/Header';
import Register from './components/Register';
import Home from './components/Home';
import Content from './components/Content';
import Post from './components/Post';


function App() {
  return (
    <div className="App">
      <Header />
      <Routes>

        <Route path="/login" element={
          <Login />
        } />

        <Route path="/register" element={
          <Register />
        } />  

        <Route path="/" element={
          <Home />
        } />  

        <Route path="/content" element={
          <Content />
        } />  

        <Route path="/post" element={
          <Post />
        } />  


      </Routes>
      
    </div>
  );
}

export default App;
