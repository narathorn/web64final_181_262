import * as React from 'react';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';


const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const theme = createTheme();

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function Album() {
  return (
      <Box>
      <CssBaseline />
      <main>
        <Box
          sx={{
            bgcolor: 'background.paper',
            pt: 6,
            pb: 6,
          }}
        >
          <Container maxWidth="sm">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom
            >
              Anime Review
            </Typography>
            <Typography variant="h5" align="center" color="text.secondary" paragraph>
              Do you need review your favorite anime ?
            </Typography>
            <Stack
              sx={{ pt: 4 }}
              direction="row"
              spacing={2}
              justifyContent="center"
            >
              <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              href="post"
            >
              +Post
            </Button>
            </Stack>
          </Container>
        </Box>

        <Container sx={{ py: 8 }} maxWidth="md">
          <Grid container spacing={4}>
            {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((card) => (
              <Grid item xs={12} sm={6} md={4}>
                <item>
                <Card
                  sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                >
                  <CardMedia
                    component="img"
                    sx={{
                      pt: '56.25%',
                    }}
                    image='https://i.ytimg.com/vi/BeuzeUyjks0/maxresdefault.jpg'
                    alt="naruto"
                  />
                  <CardContent sx={{ flexGrow: 1 }}>
                    <Link href="Content">
                    <Typography gutterBottom variant="h5" component="h2">
                      Naruto
                    </Typography>
                    </Link>
                    <Typography>
                    สิบสองปีก่อนปฐมบทแห่งนารูโตะ มีปีศาจตนหนึ่งซึ่งถูกขนานนามว่า “จิ้งจอกเก้าหาง”
                    ได้เข้าโจมตีหมู่บ้านโคโคฮะ มันทำร้ายผู้คนล้มตายเป็นจำนวนมาก 
                    โฮคาเงะรุ่นที่ 4 ผู้นำแห่งหมู่บ้านโคโนฮะ ได้เสียสละชีวิตของเขาและผนึกปีศาจจิ้งจอกเก้าหาง
                    ไว้ในตัวทารก นามว่า “อุสึมากิ นารูโตะ”
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small" href="content">View</Button>
                  </CardActions>
                </Card>
                </item>
              </Grid>
            ))}
          </Grid>
        </Container>
      </main>
      <Box sx={{ bgcolor: 'background.paper', p: 6 }} component="footer">
        <Typography variant="h6" align="center" gutterBottom>
        Faculty of providers
        </Typography>
        <Typography
          variant="subtitle1"
          align="center"
          color="text.secondary"
          component="p"
        >
          Backend by Mr.Narathorn Noophum &
          Frontend by Miss Piyawan Arakkunakorn 
        </Typography>
      </Box>
      </Box>
  );
}

