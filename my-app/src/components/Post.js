import * as React from 'react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Button from '@mui/material/Button';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import { height } from '@mui/system';
import AddToPhotosOutlinedIcon from '@mui/icons-material/AddToPhotosOutlined';
import Paper from '@mui/material/Paper';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';


export default function FullWidthTextField() {
  return (

    <Container component="main" maxWidth="xs">
        <CssBaseline />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

        <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom
            >
              Anime Review
            </Typography>

    <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
      <TextareaAutosize
      aria-label="minimum height"
      minRows={3}
      placeholder="Review Your Anime"
      style={{ width: 500 ,height: 200  }}
    />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

    <Link href="#">
    <Paper variant="outlined" sx={{width: 210 ,height: 210}}>
        <AddToPhotosOutlinedIcon sx={{ width:180,height:180}}/>
        Add Photo
    </Paper>
    </Link>

      <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              href="/"
            >
              Post
            </Button>
    </Box>
    </Container>
  );
}
